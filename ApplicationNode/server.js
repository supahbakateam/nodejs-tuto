var http = require('http');
var url = require('url');
var profile = ['bob','willy', 'fred'];
//avec express
var bodyParser = require('body-parser')
var express = require('express');
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/', (req, res) => {
    // res.setHeader('Content-Type', 'text/html');
    server.getConnections((error, count) => {
        // res.sendStatus(200);
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end('Bienvenue');
    })
});

app.get('/salut/:test', (req, res) => {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(`Bienvenue <h1>${req.params.test}</h1>`);
});

app.get('/profile', (req, res) => {
    res.render('profiles.ejs', {profiles:profile});
});

app.get('/profile/:id/details', function(req, res) {
    var id = parseInt(req.params.id);
    res.render('profile.ejs', {profile:profile[id]});
});

app.post('/profile/:id/details', function(req, res) {
    var id = parseInt(req.params.id);
    var name = req.body.Profil;
    if(name && profile[id]){
        profile[id] = name;
    }
    res.redirect('/profile');
});

var server = http.createServer(app);

server.listen(8080);





//sans express

// var server = http.createServer((req, res) => {
//     server.getConnections((error, count) => {
//         res.writeHead(200, {"Content-Type": "text/html"});
//         res.end('Nb connection : ');
//     })
// })